#pragma once

#include <vector>
#include <string>
#include "json.h"

#define SUCCESS 0
#define ERROR 1

/*
Exemple of json:
{
	"data": ["mot1", "mot2", "toto", "zoro", "aussi", "zoro"],
	"descending": false,
	"remove_duplicate": true
}
*/

//brief: a class who sort words and can remove duplicates from a json
class word_manager
{
public:
	//input: path of the json who contain the parameters
	word_manager(const std::string& path_json);

	//return: 0 if ok and 1 if nok 
	int file_well_loaded();

	//return: 0 if ok and 1 if nok 
	int process();

	//input: path of the json who will store the output
	//return: 0 if ok and 1 if nok 
	int save_data(const std::string& path_json);

	//brief: display the data stored in the class
	void display_data();
private:
	int sort_words();
	int sort_descending_words();
	int remove_duplicate();

	std::vector<std::string> m_my_words;
	Json::Value m_json_input;
	bool m_descending;
	bool m_remove_duplicate;

};

