# Word Manager

## Introduction

Word Manager is a C++ program that processes a list of words from a JSON file, sorts them, and optionally removes duplicates. The program uses the provided parameters in the JSON file to determine the processing steps.

## Table of Contents

- [Usage](#usage)
- [JSON Input Format](#json-input-format)
- [Building and Running](#building-and-running)

## Usage

To use the Word Manager program, provide the path to the input JSON file and the desired output JSON file as command-line arguments. The program will read the input parameters from the JSON file, process the words accordingly, and save the results in the output JSON file.

Example usage:

```word_manager.exe input.json output.json```

## JSON Input Format

The input JSON file should have the following format:

{
    "data": ["word1", "word2", "word3", ...],
    "descending": true,
    "remove_duplicate": true
}

    "data": An array of words to be processed.
    "descending": A boolean indicating whether to sort the words in descending order (true or false).
    "remove_duplicate": A boolean indicating whether to remove duplicate words (true or false).


## Building and Running

Use of visual studio 2022