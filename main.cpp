#include <iostream>
#include "word_manager.h"

//brief: To make the software work it needs the path of the input JSON and the path of the output JSON
int main(int argc, char* argv[]) {
    if (argc != 3)
    {
        std::cout << "ERROR in argument, enter json input path and json output path" << std::endl;
        std::cout << "Usage: " << argv[0] << " <input_json_path> <output_json_path>\n";
        return ERROR;
    }
    word_manager my_world_manager(argv[1]);
    if (my_world_manager.file_well_loaded() != SUCCESS)
    {
        std::cout << "ERROR: Failed to load file\n";
        return ERROR;
    }

    std::cout << "Data raw:\n";
    my_world_manager.display_data();

    if (my_world_manager.process() != SUCCESS)
    {
        std::cout << "ERROR: Failed to process data\n";
        return ERROR;
    }

    std::cout << "Data sorted:\n";
    my_world_manager.display_data();
    if (my_world_manager.save_data(argv[2]) != SUCCESS)
    {
        std::cout << "ERROR: Failed to write output file\n";
        return ERROR;
    }

    return SUCCESS;
}