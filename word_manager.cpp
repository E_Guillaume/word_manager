#include "word_manager.h"
#include <algorithm>
#include <iostream>
#include <fstream>

#define KEY_DATA "data"
#define KEY_DESCENDING "descending"
#define KEY_REMOVE_DUPLICATE "remove_duplicate"

word_manager::word_manager(const std::string& path_json)
{
	m_descending = 0;
	m_remove_duplicate = 0;

	std::ifstream ifs(path_json);
	Json::Reader reader;
	if (ifs.fail())
	{
		std::cout << "ERROR: Input file doesn't exist: " << std::endl;
		return;
	}
	reader.parse(ifs, m_json_input);

	if (m_json_input[KEY_DESCENDING].type() == Json::ValueType::booleanValue)
	{
		m_descending = m_json_input[KEY_DESCENDING].asBool();
	}
	else
	{
		std::cout << "ERROR: m_descending empty: " << m_json_input[KEY_DESCENDING].type() << std::endl;
		return;
	}

	if (m_json_input[KEY_REMOVE_DUPLICATE].type() == Json::ValueType::booleanValue)
	{
		m_remove_duplicate = m_json_input[KEY_REMOVE_DUPLICATE].asBool();
	}
	else
	{
		std::cout << "ERROR: m_remove_duplicate empty" << std::endl;
		return;
	}

	if (m_json_input[KEY_DATA].type() != Json::ValueType::arrayValue)
	{
		std::cout << "ERROR: Data empty" << m_json_input[KEY_DATA].type() << std::endl;
		return;
	}

	if (m_json_input[KEY_DATA].size() == 0)
	{
		return;
	}

	for (int i = 0; i < m_json_input[KEY_DATA].size(); i++)
	{
		m_my_words.push_back(m_json_input[KEY_DATA][i].asString());
	}
}

int word_manager::file_well_loaded()
{
	if (m_my_words.size() == 0)
	{
		return ERROR;
	}
	return SUCCESS;
}

struct greater
{
	template<class T>
	bool operator()(T const& a, T const& b) const { return a > b; }
};

int word_manager::sort_words()
{
	std::sort(m_my_words.begin(), m_my_words.end());
	return SUCCESS;
}

int word_manager::process()
{
	if (m_my_words.size() == 0)
	{
		return ERROR;
	}

	if (m_descending == 1)
	{
		sort_descending_words();
	}
	else
	{
		sort_words();
	}

	if (m_remove_duplicate == 1)
	{
		remove_duplicate();
	}
	return SUCCESS;
}

void word_manager::display_data()
{
	std::cout << "my vector: ";
	for (auto i : m_my_words)
	{
		std::cout << i << ", ";
	}
	std::cout << std::endl;
}

int word_manager::sort_descending_words()
{
	std::sort(m_my_words.begin(), m_my_words.end(), greater());
	return SUCCESS;
}

int word_manager::remove_duplicate()
{
	m_my_words.erase(unique(m_my_words.begin(), m_my_words.end()), m_my_words.end());
	return SUCCESS;
}

int word_manager::save_data(const std::string& path_json)
{
	Json::Value json_output;
	for (auto i : m_my_words)
	{
		json_output[KEY_DATA].append(i);
	}

	std::ofstream file_id;
	file_id.open(path_json);


	Json::StyledWriter styledWriter;
	file_id << styledWriter.write(json_output);

	file_id.close();

	if (!file_id)
	{
		std::cout << "ERROR: Writing output file" << std::endl;
		return ERROR;
	}


	return SUCCESS;
}